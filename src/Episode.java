import java.time.LocalDate;

public class Episode{
    private String name;
    private int number;
    private Season theSeason;//should link to season
    private String overview;
    private LocalDate date;
    private String tvdbImage;
    private String tmdbImage;

    public Episode () {
        this.name = null;
        this.number = -1;
        this.theSeason = null;
        this.overview = null;
        this.date = null;
        this.tvdbImage = null;
        this.tmdbImage = null;
    }
    
    public void setName ( String newName ) {
        this.name = newName;
    }
    
    public String getName () {
        return this.name;
    }
    
    public void setNumber ( int newNumber ) {
        this.number = newNumber;
    }
    
    public int getNumber () {
        return this.number;
    }
    
    public void setSeason ( Season newSeason ) {
        this.theSeason = newSeason;
    }
    
    public Season getSeason () {
        return this.theSeason;
    }
    
    public void setOverview ( String newOverview ) {
        this.overview = newOverview;
    }
    
    public String getOverview () {
        return this.overview;
    }
    
    public void setDate ( LocalDate newDate ) {
        this.date = newDate;
    }
    
    public LocalDate getDate () {
        return this.date;
    }
    
    public void setTvdbImage ( String newTvdbImage ) {
        this.tvdbImage = newTvdbImage;
    }
    
    public String getTvdbImage () {
        return this.tvdbImage;
    }

    public void setTmdbImage ( String newTmdbImage ) {
        this.tmdbImage = newTmdbImage;
    }

    public String getTmdbImage () {
        return this.tmdbImage;
    }

    public void setDateFromString ( String newDateString ) {
        if ( newDateString.matches( "\\d{4}-\\d{2}-\\d{2}" ) ) {
            LocalDate newDate = LocalDate.parse( newDateString );
            this.setDate( newDate ); 
        }
    }
    
    public String toString(){
        return "[name:" + this.name + ", number:" + this.number +
               ", season:" + this.theSeason + ", date:" +
               this.date + ", tvdbImage:" + this.tvdbImage +
                ", tmdbImage:" + tmdbImage + "]";
    }
}