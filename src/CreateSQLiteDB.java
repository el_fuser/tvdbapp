import java.sql.Connection;
import java.sql.Statement;

import java.time.LocalDate;
import java.util.ArrayList;


public class CreateSQLiteDB{
    public static void main ( String args[] ) {
        
        String DB_NAME = ".tvshows.db";//this needs to be saved somewhere else
    
        ArrayList<String> genreItems = getGenreCategories();
        ArrayList<String> ratingItems = getRatingCategories();
    
    
        try {
            Connection conn = Utilities.getSqliteConnection(DB_NAME);


            SetSeriesTable( conn );
            SetSeasonTable( conn );
            SetEpisodesTable( conn );
            SetEachGenreTable( conn );


            PrepareTable(conn, "GENRE", genreItems);
            PrepareTable(conn, "RATING", ratingItems);


            conn.close();
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            e.printStackTrace();
        }
  }

    /*
     * Set and fill simple table which is works as a repository of categories
     *
     * @param dbName    The name of the sqlite database
     * @param tableName The name of the new table
     * @param itemList  The items that wil populate the table
     */
    private static void PrepareTable ( Connection conn, String tableName,
                                       ArrayList<String> itemList )
                                       throws Exception {
        SetSimpleTable(conn, tableName);
        FillSimpleTable(conn, tableName, itemList);
    }
  
    /*
     * Creates simple table where it only has 2 colummns the tableName_id
     * and the name of the item
     *
     * @param conn      The Connection with the sqlite database
     * @param tableName The name of the new table, the first row will be
     *                  [tableName]_id and the second [tableName]_name
     */
    private static void SetSimpleTable ( Connection conn, String tableName )
                                         throws Exception {
        Statement stmt = conn.createStatement();
        String sql = "CREATE TABLE "+tableName+" " +
                     "("+tableName.toLowerCase()+"_id     INTEGER PRIMARY KEY,"+
                     " "+tableName.toLowerCase()+"_name   TEXT);";
        stmt.executeUpdate(sql);
        stmt.close();
  }
  
  /*
   * Fills simple table with items specified in itemList
   *
   * @param conn      The connection to the sqlite database
   * @param tableName The name of the table to be filled
   * @param itemList  The items that will fill the table
   */
    private static void FillSimpleTable ( Connection conn, String tableName,
                                          ArrayList<String> itemList )
                                          throws Exception {
        conn.setAutoCommit(false);
        Statement stmt = conn.createStatement();
    
        String sql = "INSERT INTO '"+tableName+"' " +
                     "SELECT NULL AS '"+tableName.toLowerCase()+"_id','"
                     +itemList.get(0)+"' AS '"+tableName.toLowerCase()+"_name' ";

        for(int i=1; i<itemList.size()-1; i++){
            sql = sql+"UNION SELECT NULL,'"+itemList.get(i)+"' ";
         }
         sql = sql+"UNION SELECT NULL,'"+itemList.get(itemList.size()-1)+"';";
      
        stmt.executeUpdate(sql);
        stmt.close();
        conn.commit();
    }
  
    /*
     * Creates table series which will hold the info about series
     *
     * @param   conn    The connection to the sqlite database
     */
    private static void SetSeriesTable ( Connection conn ) throws Exception {
        Statement stmt = conn.createStatement();
        String sql = "CREATE TABLE SERIES " +
                     "(series_id            INTEGER PRIMARY KEY     NOT NULL," +
                     " series_name          TEXT    NULL, " +
                     " series_status        TEXT    NULL, " +
                     " series_tvdb_banner   TEXT    NULL, " +
                     " series_tvdb_backdrop TEXT    NULL, " +
                     " series_tvdb_poster   TEXT    NULL, " +
                     " series_tmdb_backdrop TEXT    NULL, " +
                     " series_tmdb_poster   TEXT    NULL, " +
                     " series_tvdb_id       INTEGER NULL, " +
                     " series_tmdb_id       INTEGER NULL, " +
                     " series_date          DATE    NULL, " +
                     " series_network       TEXT    NULL, " +
                     " series_rating_id     INTEGER NULL, " +
                     " FOREIGN KEY(series_rating_id) REFERENCES RATING(rating_id)  " +
                     " ); ";
        stmt.executeUpdate( sql );
        stmt.close();
    }
  
    /*
     * Creates table episodes which will hold the info about episodes
     *
     * @param   conn    The connection to the sqlite database
     */
    private static void SetEpisodesTable ( Connection conn ) throws Exception {
        Statement stmt = conn.createStatement();
        String sql = "CREATE TABLE EPISODES " +
                     "(episode_id           INTEGER PRIMARY KEY     NOT NULL," +
                     " episode_name         TEXT    NULL, " +
                     " episode_number       INTEGER NULL, " +
                     " episode_overview     TEXT    NULL, " +
                     " episode_date         DATE    NULL, " +
                     " episode_tvdb_image   TEXT    NULL, " +
                     " episode_tmdb_image   TEXT    NULL," +
                     " episode_season_id    INTEGER NULL, " +
                     " FOREIGN KEY(episode_season_id) REFERENCES SEASON(season_id)" +
                     " ); ";
        stmt.executeUpdate( sql );
        stmt.close();
     }

    /*
     *
     */
    public static void SetEachGenreTable (  Connection conn ) throws Exception {
        Statement stmt = conn.createStatement();
        String sql = "CREATE TABLE EACHGENRE " +
                     "(eachgenre_id        INTEGER PRIMARY KEY     NOT NULL," +
                     " eachgenre_series_id INTEGER    NULL, " +
                     " eachgenre_genre_id        INTEGER NULL, " +
                     " FOREIGN KEY(eachgenre_series_id) REFERENCES SERIES(series_id)," +
                     " FOREIGN KEY(eachgenre_genre_id) REFERENCES GENRE(genre_id)" +
                     " ); ";
        stmt.executeUpdate( sql );
        stmt.close();
    }

    /*
     * Creates table season which will hold the info about season
     *
     * @param   conn    The connection to the sqlite database
     */
    private static void SetSeasonTable ( Connection conn ) throws Exception {
        Statement stmt = conn.createStatement();
        String sql = "CREATE TABLE SEASON " +
                     "(season_id            INTEGER PRIMARY KEY     NOT NULL," +
                     " season_number        INTEGER NULL, " +
                     " season_tvdb_poster   TEXT    NULL, " +
                     " season_tmdb_poster   TEXT    NULL," +
                     " season_series_id     INTEGER NULL, " +
                     " FOREIGN KEY(season_series_id) REFERENCES SERIES(series_id)" +
                     " ); ";
        stmt.executeUpdate( sql );
        stmt.close();
    }
  
    private static ArrayList<String> getGenreCategories(){
        ArrayList<String> result = new ArrayList<String>();
        result.add( "ACTION" );
        result.add( "ADVENTURE" );
        result.add( "ANIMATION" );
        result.add( "CHILDREN" );
        result.add( "COMEDY" );
        result.add( "CRIME" );
        result.add( "DOCUMENTARY" );
        result.add( "DRAMA" );
        result.add( "FAMILY" );
        result.add( "FANTASY" );
        result.add( "FOOD" );
        result.add( "GAME SHOW" );
        result.add( "HOME AND GARDEN" );
        result.add( "HORROR" );
        result.add( "MINI-SERIES" );
        result.add( "MYSTERY" );
        result.add( "NEWS" );
        result.add( "REALITY" );
        result.add( "ROMANCE" );
        result.add( "SCIENCE-FICTION" );
        result.add( "SOAP" );
        result.add( "SPECIAL INTEREST" );
        result.add( "SPORT" );
        result.add( "SUSPENSE" );
        result.add( "TALK SHOW" );
        result.add( "THRILLER" );
        result.add( "TRAVEL" );
        result.add( "WESTERN" );
        result.add( "UNDEFINED" );

        return result;
    }
  
    private static ArrayList<String> getRatingCategories(){
        ArrayList<String> result = new ArrayList<String>();

        result.add("TV-Y");
        result.add("TV-Y7");
        result.add("TV-G");
        result.add("TV-PG");
        result.add("TV-14");
        result.add("TV-MA");
        result.add("UNDEFINED");

        return result;
    }
}