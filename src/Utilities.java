import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
//import java.sql.Statement;
//import java.util.Properties;

import org.sqlite.SQLiteConfig;


public final class Utilities {
    private Utilities(){}
  
   /*
    * Tries to create a connection to a SQLite database. If for some reason
    * it does not work it thorws a connection. handle it in a function that
    * calls this one.
    *
    * @param dbName    The name of the database to which to connect
    * @return          A successful connection, otherwise throws an Exception
    */
    public static Connection getSqliteConnection( String dbName ) {
        try {
            Class.forName("org.sqlite.JDBC");

            SQLiteConfig config = new SQLiteConfig();
            config.enforceForeignKeys(true);

            Connection conn = DriverManager.getConnection("jdbc:sqlite:" + dbName,
                    config.toProperties());
            conn.setAutoCommit(false);
            return conn;
        } catch ( ClassNotFoundException e ) {
            System.exit(0);
            return null;
        } catch ( SQLException e ) {
            System.exit(0);
            return null;
        }
    }
}