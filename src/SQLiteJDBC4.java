import java.sql.*;

public class SQLiteJDBC4
{
  public static void main( String args[] )
  {
    Connection c = null;
    Statement stmt = null;
    try {
      Class.forName("org.sqlite.JDBC");
      c = DriverManager.getConnection("jdbc:sqlite:.tvshows.db");
      c.setAutoCommit(false);
      System.out.println("Opened database successfully");

      stmt = c.createStatement();
      
      
      ResultSet rs = stmt.executeQuery( "SELECT * FROM GENRE WHERE genre_name IS 'Comedy';" );
      while( rs.next() ){
        System.out.println(rs.getString("genre_id"));
      }
      
      
      /*ResultSet rs = stmt.executeQuery( "SELECT * FROM SERIES;" );
        while ( rs.next() ) {
         
        int id = rs.getInt("series_id");
        String name = rs.getString("series_name"); 
        String status = rs.getString("series_status"); 
        String banner= rs.getString("series_banner"); 
        String fanart = rs.getString("series_fanart");
        String poster= rs.getString("series_poster");
        String tvdb= rs.getString("series_tvdb_id");
        String firstaired= rs.getString("series_firstaired");
        String network= rs.getString("series_network");
        int rating_id = rs.getInt("series_rating_id");
        int genre_id = rs.getInt("series_genre_id");

        String rating_idS = rs.getString("series_rating_id");
        String genre_idS = rs.getString("series_genre_id");
         
         
         
         
         
         System.out.println( "ID = " + id );
         System.out.println( "NAME = " + name );
         System.out.println( "STATUS = " + status );
         System.out.println( "BANNER = " + banner );
         System.out.println( "FANART = " + fanart );
         System.out.println( "POSTER = " + poster );
         System.out.println( "TVDB = " + tvdb );
         System.out.println( "FIRSTAIRED = " + firstaired );
         System.out.println( "NETWORK = " + network );
         System.out.println( "ID = " + rating_id +" "+ rating_idS);
         System.out.println( "GENRE = " + genre_id +" "+ genre_idS );
         System.out.println();
      }*/
      rs.close();
      stmt.close();
      c.close();
    } catch ( Exception e ) {
      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
      System.exit(0);
    }
    System.out.println("Operation done successfully");
  }
}