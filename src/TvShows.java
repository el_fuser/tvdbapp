import java.util.ArrayList;
import java.util.Scanner;

public class TvShows{
    public static void main(String[] args)throws Exception{
        String TVDB_API_KEY = "DAB5662103E2BCF1";
        String TMDB_API_KEY = "232c7b4c06803874082a81c3ca462153";
        boolean IS_TVDB = true;
        
        /*URL oracle = new URL("http://thetvdb.com/api/"+API_KEY+"/mirrors.xml");
        BufferedReader in = new BufferedReader(
        new InputStreamReader(oracle.openStream()));

        String inputLine;
        while ((inputLine = in.readLine()) != null)
            System.out.println(inputLine);
        in.close();
        System.out.println();
        System.out.println();*/
        
        // TESTER OF SOFTWARE //

        // user searches for tv show
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Enter name of TVSHOW");
        String entry = keyboard.nextLine();
        
        // user is given choices of Series
        ArrayList<Series> result;
        if ( IS_TVDB ) {
            result = Series.getTvdbSeries(entry);
        }else {
            result = Series.getTmdbSeries ( entry, TMDB_API_KEY );
        }

        for ( int s=0;s<result.size();s++ ) {//Series s:result){
            System.out.println( (s+1)+" "+result.get(s) );
        }
        
        // user chooses one Series
        int myint = keyboard.nextInt();
        Series selected = result.get(myint-1);
        System.out.println( selected.getName() );
        
        
        // user is given full information and their db is filled
        if ( IS_TVDB ) {
            selected.retrieveFullTvdbInfo( TVDB_API_KEY );
        }else{
            selected.retrieveFullTmdbInfo( TMDB_API_KEY );
        }

        //SQLiteJDBC3.addFullSeriesToDb( selected );
        /*System.out.println(selected);
        for ( int i=0;i<selected.getSeasons().size();i++ ) {
            System.out.println( selected.getSeasons().get( i ) );
            for ( int j=0;j<selected.getSeasons().get(i).getEpisodes().size();j++ ) {
                System.out.println("\t"+selected.getSeasons().get(i).getEpisodes().get(j));
            }
        }*/
    }
}

// javac -d bin -sourcepath src -cp "lib/*" src/TvShows.java
// 
// java -cp bin:lib/* TvShows
