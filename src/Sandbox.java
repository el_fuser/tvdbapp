/**
 * Created by juliofdiaz on 10/21/14.
 */
public class Sandbox {
    public static void main ( String [] args ) {
        String txt = "123456789";
        Integer start = 4;
        Integer end = 7;

        String newN = "";
        for ( int i=start; i<=end;i++ ) {
            newN = "N"+newN;
        }

        System.out.println( txt );
        System.out.println( txt.substring(0,start-1) + newN + txt.substring(end) );
        System.out.println( txt.substring( start-1,end ) );
    }
}
