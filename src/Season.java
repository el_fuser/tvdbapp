import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

public class Season{
    private Series series;
    private int number;
    private String tvdbPoster;
    private String tmdbPoster;
    private ArrayList<Episode> episodes;
    
    public Season () {
        this.series = null;
        this.number = -1;
        this.tvdbPoster = null;
        this.tmdbPoster = null;
        this.episodes = new ArrayList<Episode>();
    }

    public void setSeries ( Series newSeries ) {
        this.series = newSeries;
    }

    public Series getSeries () {
        return this.series;
    }

    public void setNumber ( int newNumber ) {
        this.number = newNumber;
    }
    
    public int getNumber () {
        return this.number;
    }
    
    public void setTvdbPoster (String newTvdbPoster) {
        this.tvdbPoster = newTvdbPoster;
    }
    
    public String getTvdbPoster () {
        return this.tvdbPoster;
    }

    public void setTmdbPoster ( String newTmdbPoster ) {
        this.tmdbPoster = newTmdbPoster;
    }

    public String getTmdbPoster () {
        return this.tmdbPoster;
    }

    public void setEpisodes ( ArrayList<Episode> newEpisodes ) {
        this.episodes = newEpisodes;
    }

    public ArrayList<Episode> getEpisodes () {
        return this.episodes;
    }

    /**
     * This method acquires all the Episodes in a Season from tmdb as long as the season
     * has a number and the season holds a series with a tmdb id
     *
     * @param   tmdbApiKey  the developers api key
     */
    public void setTmdbEpisodes ( String tmdbApiKey ) {
        if ( this.number == -1 || this.series.getTmdbId() == -1 ) {
            this.setEpisodes(new ArrayList<Episode>());
        }

        String url = "https://api.themoviedb.org/3/tv/" + this.series.getTmdbId() +
                "/season/" + this.number + "?api_key=" + tmdbApiKey;
        String str = parseJsonUrl( url );

        try {
            JSONObject theJsonObject = (JSONObject) JSONValue.parseWithException(str);

            JSONArray genreJsonArray = (JSONArray) theJsonObject.get( "episodes" );
            for ( Object o : genreJsonArray ) {
                JSONObject tempGenreJsonObject = (JSONObject) o;
                Episode tempEpisode = new Episode();

                String tempEpisodeName = getJsonObjectText( tempGenreJsonObject, "name" );
                tempEpisode.setName(tempEpisodeName);

                String tempEpisodeNumberString = getJsonObjectText( tempGenreJsonObject,
                        "episode_number" );
                int tempEpisodeNumber = Integer.parseInt( tempEpisodeNumberString );
                tempEpisode.setNumber(tempEpisodeNumber);

                String tempEpisodeOverview = getJsonObjectText( tempGenreJsonObject,
                        "overview" );
                tempEpisode.setOverview(tempEpisodeOverview);

                String tempEpisodeDateString = getJsonObjectText( tempGenreJsonObject,
                        "air_date" );
                tempEpisode.setDateFromString(tempEpisodeDateString);

                String tempEpisodeTmdbImage = getJsonObjectText( tempGenreJsonObject,
                        "still_path" );
                tempEpisode.setTmdbImage( tempEpisodeTmdbImage );

                tempEpisode.setSeason( this );

                this.episodes.add(tempEpisode);
            }

        } catch ( ParseException e ) {
            System.exit( 0 );
            //email julio because json is not formatted correctly
        }
    }

    /**
     * Gets a url containing a Json and returns it as a String
     *
     * @param   url     the url of the Json
     */
    private static String parseJsonUrl ( String url ) {
        String result = "";

        try{
            URL u = new URL( url );
            Scanner in = new Scanner( u.openStream() );
            while ( in.hasNext() ){
                result += in.nextLine();
            }
            in.close();
        }catch( IOException e ){
            result = "{\"page\":1,\"results\":[],"+
                    "\"total_pages\":0,\"total_results\":0}";
            //If this doesn't work get info from tvdb
        }

        return result;
    }

    /* SHOULD PUT IN UTILS
     * Retrieves the text content of a JSONObject
     *
     * @param   theJsonObject   the JSONObject that will get its content retrieved
     * @param   elementName     The name of the tag to get the text
     * @return                  The text content of a tag of a JSONObject
     */
    private static String getJsonObjectText ( JSONObject theJsonObject,
                                              String tagName ) {
        try{
            return theJsonObject.get( tagName ).toString();
        }catch ( Exception e ) {
            return "null";
        }
    }

    public String toString(){
        return "[number:" + this.number + ", tvdbPoster:" + this.tvdbPoster +
                ", tmdbPoster:" + this.tmdbPoster + "]";
    }

    public boolean equals(Object other){
        Season anotherSeason = (Season)other;

        if ( !( other instanceof Season ) ) {
            return false;
        }

        boolean isSameNumber = this.number == anotherSeason.getNumber();
        boolean isSameSeries =  this.series.getName().equals( anotherSeason.
                                                              getSeries().getName() );

        return isSameNumber && isSameSeries;
    }
}